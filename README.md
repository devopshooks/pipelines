# Pipelines

## Docker

### Docker build
```
# Import DevOpsHooks SharedLib
include:
  - project: devopshooks/pipelines
    ref: main
    file:
      - docker/build.yml
build:
 only:
   - tags
   - dev
   - beta
 except:
   - main
```